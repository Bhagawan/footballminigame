package com.example.footballminigame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.footballminigame.R
import com.example.footballminigame.util.DrawElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class FootballGameView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var bubbleWidth = 0.0f
    private val plankWidth = 10.0f

    private val restartBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_baseline_replay)?.toBitmap()
    private val ballBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ball)
    private val gateBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.gate)
    private val playerBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.player_default)
    private val goalkeeperBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.goalkeeper)
    private val lineBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.line)
    private val commandsBitmaps = listOf(
        Pair(BitmapFactory.decodeResource(context.resources, R.drawable.arsenal), "Арсенал"),
        Pair(BitmapFactory.decodeResource(context.resources, R.drawable.barselona), "Барселона"),
        Pair(BitmapFactory.decodeResource(context.resources, R.drawable.bayern_munich), "Bayern Munich"),
        Pair(BitmapFactory.decodeResource(context.resources, R.drawable.madrid), "Мадрид"),
        Pair(BitmapFactory.decodeResource(context.resources, R.drawable.manchester), "Манчестер")
    )

    private var gate: DrawElement? = null
    private var ball: DrawElement? = null
    private var defenders = ArrayList<DrawElement>()

    private var goalTargetX = 0.0f
    private var goalTargetY = 0.0f
    private var goalTargetSize = 0.0f

    private var scoreYou = 0
    private var scoreEnemy = 0
    private var yourCommand = 0
    private var enemyCommand = 1
    private var hitNumber = 0

    private var sliderPercent = 50
    private var sliderDelta = 1
    private var sliderHitTimer = -1

    private var hitTargetX = 0.0f
    private var hitTargetY = 0.0f
    private var hitTimer = 0
    private val hitAnimMaxTime = 90 // 1.5 sec

    private var enemyTurnTimer = 0
    private var enemyHit : Boolean? = null

    private var state = AIM

    companion object {
        const val AIM = 0
        const val HIT_ANIMATION = 1
        const val ENEMY_TURN = 2
        const val END_SCREEN = 3
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bubbleWidth = (mHeight - plankWidth) / 8.0f
            if(ball == null) ball = DrawElement(ballBitmap, mWidth * 0.4f, mHeight - mWidth * 0.1f, mWidth * 0.2f, mWidth * 0.2f)
            if(gate == null) gate = DrawElement(gateBitmap, mWidth* 0.1f, mHeight * 0.6f, mWidth * 0.75f, mHeight * 0.2f)
            restart()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
            drawUI(it)
            when(state) {
                AIM -> {
                    updateSliders()
                    drawSliders(it)
                }
                HIT_ANIMATION -> updateHitAnimation()
                ENEMY_TURN -> drawNotification(it)
                END_SCREEN -> drawWinScreen(it)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    END_SCREEN -> restart()
                    AIM -> sliderHitTimer = 0
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public


    fun getState() = FootballGameState(ball, defenders, goalTargetX, goalTargetY, goalTargetSize, scoreYou, scoreEnemy, yourCommand, enemyCommand
        , hitNumber, sliderPercent, sliderDelta, sliderHitTimer, hitTargetX, hitTargetY, hitTimer, enemyTurnTimer, enemyHit, state)

    fun setState(gameState: FootballGameState) {
        ball = gameState.ball
        defenders = gameState.defenders
        goalTargetX = gameState.goalTargetX
        goalTargetY = gameState.goalTargetY
        goalTargetSize = gameState.goalTargetSize
        scoreYou = gameState.scoreYou
        scoreEnemy = gameState.scoreEnemy
        yourCommand = gameState.commandYou
        enemyCommand = gameState.commandEnemy
        hitNumber = gameState.hitNumber
        sliderPercent = gameState.sliderPercent
        sliderDelta = gameState.sliderDelta
        sliderHitTimer = gameState.sliderHitTimer
        hitTargetX = gameState.hitTargetX
        hitTargetY = gameState.hitTargetY
        hitTimer = gameState.hitTimer
        enemyTurnTimer = gameState.enemyTurnTimer
        enemyHit = gameState.enemyHit
        state = gameState.state
    }

    /// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        gate?.let {
            c.drawBitmap(it.bitmap, null, Rect(it.x.toInt(), (it.y - it.height).toInt(), (it.x + it.width).toInt(), it.y.toInt()), p)
        }
        for(player in defenders) {
            c.drawBitmap(player.bitmap, null, Rect(player.x.toInt(), (player.y - player.height).toInt(), (player.x + player.width).toInt(), player.y.toInt()), p)
        }
        ball?.let {
            c.drawBitmap(it.bitmap, null, Rect(it.x.toInt(), (it.y - it.height).toInt(), (it.x + it.width).toInt(), it.y.toInt()), p)
        }
    }

    private fun drawSliders(c: Canvas) {
        val p = Paint()
        p.color = ResourcesCompat.getColor(context.resources, R.color.transparent_black, null)

        c.drawRect(mWidth * 0.1f, mHeight - mWidth * 0.09f, mWidth * 0.9f, mHeight - mWidth * 0.01f, p)
        c.drawRect(mWidth * 0.91f, mHeight * 0.4f, mWidth * 0.99f, mHeight * 0.85f, p)
        val bOne = mWidth * 0.008f
        val rOne = mHeight * 0.0045f
        c.drawBitmap(lineBitmap, null, Rect((mWidth / 2.0f - bOne * 5.0f).toInt(), (mHeight - mWidth * 0.09f).toInt(), (mWidth / 2.0f + bOne * 5.0f).toInt(), (mHeight - mWidth * 0.01f).toInt()), p)
        c.drawBitmap(lineBitmap, null, Rect((mWidth * 0.91f).toInt(), (mHeight * 0.625f - rOne * 5.0f).toInt(), (mWidth * 0.99f).toInt(), (mHeight * 0.625f + rOne * 5.0f).toInt()), p)

        p.color = Color.WHITE
        c.drawRect(mWidth * 0.1f + sliderPercent * bOne - bOne * 0.5f, mHeight - mWidth * 0.09f, mWidth * 0.1f + sliderPercent * bOne + bOne * 0.5f, mHeight - mWidth * 0.01f, p)
        c.drawRect(mWidth * 0.91f, mHeight * 0.4f + sliderPercent * rOne - rOne * 0.5f, mWidth * 0.99f, mHeight * 0.4f + sliderPercent * rOne + rOne * 0.5f, p)

        p.textAlign = Paint.Align.CENTER
        p.textSize = 20.0f
        c.drawText("Сила", mWidth * 0.95f, mHeight * 0.4f - 10, p)
        c.drawText("Точность", mWidth * 0.2f, mHeight - mWidth * 0.1f - 10, p)
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.textSize = 20.0f
        if(state != END_SCREEN) {
            p.color = ResourcesCompat.getColor(context.resources, R.color.green_light, null)
            c.drawText("Идет игра", mWidth / 2.0f, 20.0f, p)
        }
        val logoSize = mWidth * 0.2f
        p.color = Color.WHITE
        c.drawBitmap(commandsBitmaps[yourCommand].first, null, Rect((mWidth * 0.05f).toInt(),(mWidth * 0.05f).toInt(), (mWidth * 0.05f + logoSize).toInt(),(mWidth * 0.05f + logoSize).toInt()), p)
        c.drawText(commandsBitmaps[yourCommand].second, mWidth * 0.05f + logoSize * 0.5f, mWidth * 0.05f + logoSize + 20, p)
        c.drawBitmap(commandsBitmaps[enemyCommand].first, null, Rect((mWidth * 0.95f - logoSize).toInt(),(mWidth * 0.05f).toInt(), (mWidth * 0.95f).toInt(),(mWidth * 0.05f + logoSize).toInt()), p)
        c.drawText(commandsBitmaps[enemyCommand].second, mWidth * 0.95f - logoSize * 0.5f, mWidth * 0.05f + logoSize + 20, p)
        p.textSize = 90.0f
        c.drawText("$scoreYou - $scoreEnemy", mWidth / 2.0f, mWidth * 0.05f + logoSize * 0.65f, p)

        p.color = ResourcesCompat.getColor(context.resources, R.color.transparent_black, null)
        c.drawRect(-10.0f, mWidth * 0.05f + logoSize + 30, mWidth + 10.0f, mHeight * 0.35f, p)
        p.style = Paint.Style.STROKE
        p.color = Color.WHITE
        c.drawRect(-10.0f, mWidth * 0.05f + logoSize + 30, mWidth + 10.0f, mHeight * 0.35f, p)
        p.style = Paint.Style.FILL
        p.color = ResourcesCompat.getColor(context.resources, R.color.green_light, null)

        p.textSize = 30.0f
        val lineH = (mHeight * 0.35f -  (mWidth * 0.05f + logoSize + 30)) / 3
        c.drawText("Нажмите чтобы остановить", mWidth / 2.0f, (mWidth * 0.05f + logoSize + 30) + lineH / 2, p)
        c.drawText("линии на зеленом указателе", mWidth / 2.0f, (mWidth * 0.05f + logoSize + 30) + lineH * 1.5f, p)
        c.drawText("удара", mWidth / 2.0f, (mWidth * 0.05f + logoSize + 30) + lineH * 2.5f, p)

    }

    private fun drawNotification(c: Canvas) {
        if(state == ENEMY_TURN) {
            val p = Paint()
            p.textAlign = Paint.Align.CENTER
            enemyTurnTimer++
            if(enemyTurnTimer <= 30) {
                p.strokeWidth = 3.0f
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.1f, mHeight * 0.35f, mWidth * 0.9f, mHeight * 0.65f, 20.0f, 20.0f, p)
                p.style = Paint.Style.STROKE
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_light, null)
                c.drawRoundRect(mWidth * 0.1f + 5, mHeight * 0.35f + 5, mWidth * 0.9f - 5, mHeight * 0.65f - 5, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                p.textSize = 90.0f
                val t: String
                if(goalTargetX == hitTargetX && goalTargetY == hitTargetY) {
                    p.color = Color.YELLOW
                    t = "Гол!"
                } else {
                    p.color = Color.RED
                    t = "Мимо"
                }
                c.drawText(t, mWidth / 2.0f, mHeight / 2.0f, p)
            } else if(enemyTurnTimer <= 90) {
                p.strokeWidth = 3.0f
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.1f, mHeight * 0.35f, mWidth * 0.9f, mHeight * 0.65f, 20.0f, 20.0f, p)
                p.style = Paint.Style.STROKE
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_light, null)
                c.drawRoundRect(mWidth * 0.1f + 5, mHeight * 0.35f + 5, mWidth * 0.9f - 5, mHeight * 0.65f - 5, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                p.textSize = 50.0f
                c.drawText("Удар противника:", mWidth / 2.0f, mHeight * 0.4f, p)
                if(enemyTurnTimer >= 60) {
                    if(enemyHit == null) enemyHit = Random.nextInt(2) == 0
                    p.textSize = 90.0f
                    val text = if(enemyHit == true) {
                        p.color = Color.YELLOW
                        "Гол!"
                    }
                    else {
                        p.color = Color.RED
                        "Мимо"
                    }
                    c.drawText(text, mWidth / 2.0f, mHeight * 0.55f, p)
                }
            } else {
                hitNumber++
                if(goalTargetX == hitTargetX && goalTargetY == hitTargetY) scoreYou++
                if(enemyHit == true) scoreEnemy++
                enemyHit = null
                enemyTurnTimer = 0
                state = if(hitNumber >= 5) END_SCREEN
                else {
                    newDefenderPosition()
                    resetBall()
                    AIM
                }
            }
        }
    }

    private fun drawWinScreen(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.strokeWidth = 3.0f
        p.style = Paint.Style.FILL
        p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
        c.drawRoundRect(mWidth * 0.1f, mHeight * 0.25f, mWidth * 0.9f, mHeight * 0.75f, 20.0f, 20.0f, p)
        p.style = Paint.Style.STROKE
        p.color = ResourcesCompat.getColor(context.resources, R.color.grey_light, null)
        c.drawRoundRect(mWidth * 0.1f + 5, mHeight * 0.25f + 5, mWidth * 0.9f - 5, mHeight * 0.75f - 5, 20.0f, 20.0f, p)
        p.textSize = 70.0f
        p.style = Paint.Style.FILL
        val text = if(scoreEnemy > scoreYou) {
            p.color = Color.RED
            "Поражение"
        } else if(scoreEnemy < scoreYou) {
            p.color = Color.YELLOW
            "Победа!"
        } else {
            p.color = Color.WHITE
            "Ничья"
        }
        c.drawText(text, mWidth / 2.0f, mHeight * 0.31f, p)
        p.textSize = 30.0f

        val logoSize = mWidth * 0.2f
        c.drawBitmap(commandsBitmaps[yourCommand].first, null, Rect((mWidth * 0.1f + 20).toInt(), (mHeight * 0.25f + 90).toInt(), (mWidth * 0.1f + 20 + logoSize).toInt(),(mHeight * 0.25f + 90 + logoSize).toInt()), p)
        c.drawText(commandsBitmaps[yourCommand].second, mWidth * 0.1f + 30 + logoSize * 0.5f, mHeight * 0.25f + 90 + logoSize + 20, p)
        c.drawBitmap(commandsBitmaps[enemyCommand].first, null, Rect((mWidth * 0.9f - 20 - logoSize).toInt(),(mHeight * 0.25f + 90).toInt(), (mWidth * 0.9f - 20).toInt(),(mHeight * 0.25f + 90 + logoSize).toInt()), p)
        c.drawText(commandsBitmaps[enemyCommand].second, mWidth * 0.9f - 30 - logoSize * 0.5f, mHeight * 0.25f + 90 + logoSize + 20, p)

        p.textSize = 50.0f
        p.color = Color.WHITE
        c.drawText("$scoreYou - $scoreEnemy", mWidth / 2.0f, mHeight * 0.25f + 100 + logoSize * 0.75f, p)

        restartBitmap?.let {
            val s = mHeight * 0.5f - 145 - logoSize
            c.drawBitmap(it, null, Rect(((mWidth - s) / 2.0f).toInt(), (mHeight * 0.75f - 10 - s).toInt(), ((mWidth + s) / 2.0f).toInt(), (mHeight * 0.75f - 10).toInt()), p)
        }

    }

    private fun updateSliders() {
        if(sliderHitTimer >= 0) {
            sliderHitTimer ++
            if(sliderHitTimer >= 30) {
                sliderHitTimer = -1
                state = HIT_ANIMATION
                if((sliderPercent - 50).absoluteValue <= 5) {
                    hitTargetX = goalTargetX
                    hitTargetY = goalTargetY
                } else {
                    hitTargetX = goalTargetX + Random.nextInt(200,400) * if(Random.nextInt(2) == 0) 1 else -1
                    hitTargetY = goalTargetY - Random.nextInt(100,300)
                }
            }
        } else {
            if(sliderPercent + sliderDelta !in 5..95) sliderDelta *= -1
            sliderPercent += sliderDelta
        }
    }

    private fun updateHitAnimation() {
        ball?.let {
            hitTimer++
            if(hitTimer >= hitAnimMaxTime) {
                state = ENEMY_TURN
                hitTimer = 0
            } else {
                val dX : Float
                val dY : Float
                if(hitAnimMaxTime - hitTimer <  hitAnimMaxTime * 0.3f) {
                    dX = (hitTargetX - it.x) / (hitAnimMaxTime - hitTimer)
                    dY = (hitTargetY - it.y) / (hitAnimMaxTime - hitTimer)
                } else {
                    var d = -1
                    if(defenders.isNotEmpty()) {
                        if(defenders.first().x < mWidth / 3) d = 1
                    }
                    dX = ((hitTargetX - it.x) + (mWidth * 0.5f * d *  (1 - (hitTimer / (hitAnimMaxTime * 0.7f))).absoluteValue)) / (hitAnimMaxTime - hitTimer)
                    dY = ((hitTargetY - it.y) - (mHeight * 0.5f * (1 - (hitTimer / (hitAnimMaxTime * 0.7f))).absoluteValue)) / (hitAnimMaxTime - hitTimer)
                }
                it.x += dX
                it.y += dY
                val size = goalTargetSize + (mWidth * 0.2f - goalTargetSize) / hitAnimMaxTime * (hitAnimMaxTime - hitTimer)
                it.width = size
                it.height = size
            }
        }
    }

    private fun restart() {
        newDefenderPosition()

        scoreYou = 0
        scoreEnemy = 0
        hitNumber = 0
        val freeCommands = ArrayList<Int>()
        for(n in commandsBitmaps.indices) freeCommands.add(n)
        yourCommand = freeCommands.random()
        freeCommands.remove(yourCommand)
        enemyCommand = freeCommands.random()

        resetBall()

        sliderPercent = 50
        sliderDelta = 1
        sliderHitTimer = -1
        state = AIM
    }

    private fun newDefenderPosition() {
        val w = (mWidth * 0.9f) / 8.0f
        val h = playerBitmap.height / playerBitmap.width * w
        val left = Random.nextInt(2) == 0
        val dOffset = if(left) 0.0f
        else (mWidth * 0.9f) - w * 4

        defenders.clear()
        for(n in 0..3) defenders.add(DrawElement(playerBitmap, n * w + dOffset, mHeight * 0.65f, w, h))

        val gW = (mWidth * 0.75f - w * 4) / 3
        val gH = goalkeeperBitmap.height / goalkeeperBitmap.width * gW
        val gX = if(!left) mWidth * 0.1f + gW
        else w * 4 + gW
        defenders.add(DrawElement(goalkeeperBitmap, gX, mHeight * 0.6f, gW, gH))

        goalTargetSize = gW * 0.5f
        goalTargetY = mHeight * 0.6f - gH / 2
        goalTargetX = if(Random.nextInt(2) == 0) gX - goalTargetSize / 2.0f
        else gX + gW + goalTargetSize / 2.0f
    }

    private fun resetBall() {
        ball?.apply {
            x = mWidth * 0.4f
            y = mHeight - mWidth * 0.1f
            width = mWidth * 0.2f
            height = mWidth * 0.2f
        }
    }

}