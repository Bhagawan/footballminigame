package com.example.footballminigame.game

import androidx.lifecycle.ViewModel

class FootballGameViewModel: ViewModel() {
    private var gameState: FootballGameState? = null

    fun saveGameState(state: FootballGameState) {
        gameState = state
    }

    fun getGameState() : FootballGameState? = gameState
}