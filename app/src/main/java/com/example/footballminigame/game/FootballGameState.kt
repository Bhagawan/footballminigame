package com.example.footballminigame.game

import com.example.footballminigame.util.DrawElement

data class FootballGameState(val ball: DrawElement?,
                             val defenders: ArrayList<DrawElement>,
                             val goalTargetX: Float,
                             val goalTargetY: Float,
                             val goalTargetSize: Float,
                             val scoreYou: Int,
                             val scoreEnemy: Int,
                             val commandYou: Int,
                             val commandEnemy: Int,
                             val hitNumber: Int,
                             val sliderPercent: Int,
                             val sliderDelta: Int,
                             val sliderHitTimer: Int,
                             val hitTargetX: Float,
                             val hitTargetY: Float,
                             val hitTimer: Int,
                             val enemyTurnTimer: Int,
                             val enemyHit: Boolean?,
                             val state: Int)
