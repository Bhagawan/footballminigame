package com.example.footballminigame.util

import android.graphics.Bitmap

data class DrawElement(val bitmap: Bitmap, var x: Float, var y: Float, var width: Float, var height: Float)
