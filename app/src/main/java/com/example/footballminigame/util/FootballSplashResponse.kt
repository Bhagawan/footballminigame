package com.example.footballminigame.util

import androidx.annotation.Keep

@Keep
data class FootballSplashResponse(val url : String)