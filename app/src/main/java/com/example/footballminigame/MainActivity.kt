package com.example.footballminigame

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.footballminigame.databinding.ActivityMainBinding
import com.example.footballminigame.game.FootballGameViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var gameViewModel : FootballGameViewModel
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        gameViewModel = ViewModelProvider(this)[FootballGameViewModel::class.java]
        setContentView(binding.root)
        loadBackground()
    }

    override fun onResume() {
        val gameState = gameViewModel.getGameState()
        gameState?.let {
            binding.game.setState(it)
        }
        super.onResume()
    }

    override fun onPause() {
        gameViewModel.saveGameState(binding.game.getState())
        super.onPause()
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom) {
                bitmap?.let { binding.game.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/FootballMiniGame/field.png").into(target)
    }
}